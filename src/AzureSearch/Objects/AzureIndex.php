<?php

namespace Drupal\azure_searchx\AzureSearch\Objects;

use Drupal\search_api\DataType\DataTypePluginManager;

/**
 * Azure index object.
 */
class AzureIndex {

  /**
   * Index object.
   *
   * @var object
   */
  private $index;

  const AZURETYPES = [
    "text"    => "Edm.String",
    "string"  => "Edm.String",
    "integer" => "Edm.Int32",
    "double"  => "Edm.Double",
    "decimal" => "Edm.Double",
    "boolean" => "Edm.Boolean",
    "date"    => "Edm.Int64",
  ];

  /**
   * The type manager service.
   *
   * @var \Drupal\search_api\DataType\DataTypePluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an AzureIndex object.
   */
  public function __construct(DataTypePluginManager $pluginManager, String $name) {
    $this->index = new \stdClass();
    $this->index->name = $name;
    $this->pluginManager = $pluginManager;
    $this->index->fields = [];
  }

  /**
   * Add fields to index definition.
   */
  public function addField($name, $type, $key = FALSE) {
    $plugin = $this->pluginManager->getDefinition($type);
    $field_type = "";
    if (method_exists($plugin['class'], 'getAzureType')) {
      $field_type = $plugin['class']::getAzureType();
    }
    elseif (!empty(static::AZURETYPES[$type])) {
      $field_type = static::AZURETYPES[$type];
    }
    else {
      return;
    }
    $field = new \stdClass();
    $field->name = $name;
    $field->type = $field_type;
    if ($key) {
      $field->key = $key;
    }
    $this->index->fields[] = $field;
  }

  /**
   * Add boosts to index definition.
   */
  public function addScoringProfile($name, $scoring) {

    if (count((array) $scoring)) {
      if (!isset($this->index->scoringProfiles)) {
        $this->index->scoringProfiles = [];
      }

      $scoring_profile = new \stdClass();
      $scoring_profile->name = $name;
      $scoring_profile->text = new \stdClass();
      $scoring_profile->text->weights = $scoring;

      $this->index->scoringProfiles[] = $scoring_profile;
    }
  }

  /**
   * Get azure index definition object.
   *
   * @return mixed
   *   object for index creation
   */
  public function getDefinition() {
    return $this->index;
  }

}
