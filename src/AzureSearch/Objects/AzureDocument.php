<?php

namespace Drupal\azure_searchx\AzureSearch\Objects;

/**
 * Azure document object for indexing.
 */
class AzureDocument {

  /**
   * Document.
   *
   * @var object
   */
  private $document;

  /**
   * Action.
   *
   * @var string
   */
  private $action;

  /**
   * Constructs an AzureDocument object.
   */
  public function __construct(string $action) {
    $this->document = new \stdClass();
    $this->document->value = [];
    $this->action = $action;
  }

  /**
   * Add entity with fields to object.
   */
  public function addDocument($fields) {
    $doc = new \stdClass();
    $doc->{'@search.action'} = $this->action;
    foreach ($fields as $key => $value) {
      if (is_array($value)) {
        $doc->{$key} = [];
        foreach ($value as $id => $sub) {
          $doc->{$key}[$id] = $sub;
        }
      }
      else {
        $doc->{$key} = $value;
      }
    }
    $this->document->value[] = $doc;
  }

  /**
   * Get documents for indexing.
   */
  public function getDocuments() {
    return $this->document;
  }

}
