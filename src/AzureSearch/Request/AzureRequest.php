<?php

namespace Drupal\azure_searchx\AzureSearch\Request;

/**
 * Object with azure search parameters.
 */
class AzureRequest {

  /**
   * Url string.
   *
   * @var string
   */
  protected $url;

  /**
   * Key string.
   *
   * @var string
   */
  protected $key;

  /**
   * Version.
   *
   * @var string
   */
  protected $version;

  /**
   * Debug indicator.
   *
   * @var bool
   */
  protected $debug;

  /**
   * Constructs an AzureRequest object.
   */
  public function __construct($url, $key, $version = '2019-05-06') {
    $this->url = $url;
    $this->key = $key;
    $this->version = $version;
  }

  /**
   * Get http response code from azure.
   */
  public function requestCode($path, $method = 'GET', $options = []) {
    $http_options = ['api-version' => $this->version];
    $client = \Drupal::httpClient();
    $target_url = $this->url . '/' . $path . '?' . http_build_query($http_options);
    $code = -1;
    $headers = ['api-key' => $this->key];

    try {
      if ($method == 'POST' || $method == 'PUT') {
        $data = json_encode($options);
        $headers['Content-Type'] = 'application/json';
        $headers['Content-Length'] = strlen($data);
        $response = $client->{$method}($target_url, [
          'headers' => $headers,
          'body'    => $data,
        ]);
        $code = $response->getStatusCode();
      }
      elseif ($method == 'GET') {
        $response = $client->{$method}($target_url, [
          'headers' => $headers,
        ]);
        $code = $response->getStatusCode();
      }
    }
    catch (\Exception $e) {
      watchdog_exception('azure_searchx', $e);
    }

    return $code;
  }

  /**
   * Make an request to azure.
   */
  public function request($path, $method = 'GET', $options = [], &$httpcode = NULL) {
    $http_options = ['api-version' => $this->version];
    $client = \Drupal::httpClient();
    $target_url = $this->url . '/' . $path . '?' . http_build_query($http_options);
    $result = [];
    $headers = ['api-key' => $this->key];
    try {
      if (in_array($method, ['POST', 'PUT', 'DELETE'])) {
        $data = json_encode($options);
        $headers['Content-Type'] = 'application/json';
        $headers['Content-Length'] = strlen($data);
        $response = $client->{$method}($target_url, [
          'headers' => $headers,
          'body'    => $data,
        ]);
        $httpcode = $response->getStatusCode();
        $result = json_decode($response->getBody());
      }
      elseif ($method == 'GET') {
        $response = $client->{$method}($target_url, [
          'headers' => $headers,
        ]);
        $httpcode = $response->getStatusCode();
        $result = json_decode($response->getBody());
      }
    }
    catch (\Exception $e) {
      $httpcode = $e->getCode();
      watchdog_exception('azure_searchx', $e);
    }

    return $result;
  }

  /**
   * Add needed data to azure request.
   */
  protected function setRequestData(&$curl, $method, $options) {
    $headers = [];

    if ($method == 'POST' || $method == 'PUT') {
      $data = json_encode($options);

      $headers[] = 'Content-Type: application/json';
      $headers[] = 'Content-Length: ' . strlen($data);
    }

    $headers[] = 'api-key:' . $this->key;
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  }

}
