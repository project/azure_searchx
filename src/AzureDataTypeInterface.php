<?php

namespace Drupal\azure_searchx;

/**
 * Provides an interface for data type plugin managers.
 */
interface AzureDataTypeInterface {

  /**
   * Returns an azure field data type.
   *
   * @see https://docs.microsoft.com/en-us/rest/api/searchservice/supported-data-types
   *   Data types details.
   *
   * @return string
   *   Data type.
   */
  public static function getAzureType();

  /**
   * Returns an azure field data type`s collection status.
   *
   * @return bool
   *   is this field collection or not.
   */
  public static function isCollection();

  /**
   * Returns an azure field data type`s condition string.
   *
   * @param string $field_name
   *   Field name.
   * @param mixed $value
   *   Field value object.
   * @param string $operator
   *   Field sorting operator.
   *
   * @return string
   *   Condition string.
   */
  public static function fieldFilterFunction($field_name, $value, $operator);

}
