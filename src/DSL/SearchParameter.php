<?php

namespace Drupal\azure_searchx\DSL;

class SearchParameter implements ParameterAble
{
  protected $queryType;

  protected $pageSize;

  protected $allOptions;

  public function __construct($pageSize)
  {
    //use simple query syntax
    $this->queryType = 'full';
    $this->pageSize = $pageSize;

    $this->allOptions = [
      'queryType' => $this->queryType,
      'count' => true
    ];
  }

  public function construct()
  {
    return $this->allOptions;
  }

  /**
   * @param $key
   * @param $value
   * @return $this
   */
  public function setOption($key, $value)
  {
    $this->allOptions[$key] = $value;
    return $this;
  }

  public function getPageSize(){
    return $this->pageSize;
  }
}
