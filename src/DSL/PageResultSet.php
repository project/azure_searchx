<?php

namespace Drupal\azure_searchx\DSL;

class PageResultSet
{
  /**
   * @return mixed
   */
  public function getTotalPage()
  {
    return (int)(($this->pageSize) == 0 ? 1 : ceil($this->total / $this->pageSize));
  }

  public function getCollection(){
    return $this->collection;
  }

  public $page;

  public $pageSize;

  protected $collection;

  public $total;

  public function __construct($page, $total, $collection, $pageSize)
  {
    $this->page = $page;
    $this->pageSize = $pageSize;
    $this->total = $total;
    $this->collection = $collection;
  }
}
