<?php

namespace Drupal\azure_searchx\DSL;

class AzureResult implements IPage
{
  /**
   * @var SearchParameter
   */
  protected $searchParam;

  /**
   * @var SearchQueryAble
   */
  protected $query;

  public function __construct($query, $searchParam)
  {
    $this->query = $query;
    $this->searchParam = $searchParam;
  }

  public function pagination($page, $orderBy = '')
  {
    $param = $this->searchParam;
    $pageIndex = $page > 0 ? ($page - 1) * $param->getPageSize() : 0;
    $this->searchParam->setOption('skip', $pageIndex);
    $this->searchParam->setOption('top', $param->getPageSize());
    $result = $this->query->fetch($this->searchParam);
    $vars = get_object_vars($result);
    return new PageResultSet($page, $vars['@odata.count']??0, $result->value, $param->getPageSize());
  }
}
