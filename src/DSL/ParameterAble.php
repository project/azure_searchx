<?php
namespace Drupal\azure_searchx\DSL;

interface ParameterAble{
  function construct();

  function setOption($key, $value);
}
