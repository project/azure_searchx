<?php

namespace Drupal\azure_searchx\DSL;

use Drupal\azure_searchx\AzureSearch\Request\AzureRequest;
use Drupal\azure_searchx\DSL\filters\AzureFilter;

class AzureQuery implements SearchQueryAble
{
  /**
   * @var AzureRequest
   */
  protected $request;

  protected $indexer;

  protected $indexerUri;

  protected $parameter;

  public function __construct($request, $indexer, $pageSize = 10)
  {
    $this->request = $request;
    $this->indexer = $indexer;
    $this->indexerUri = "indexes/$indexer/docs/search";

    $this->parameter = new SearchParameter($pageSize);
  }

  public function searchFields($value)
  {
    $this->parameter->setOption('searchFields', $value);
    return $this;
  }

  /**
   * @param $filterQuery AzureFilter
   * @param string $search
   * @return array|mixed
   */
  public function resultSet($filterQuery, $search = '')
  {
    if (!empty($search)){
      $this->parameter->setOption('search', "/.*$search*./");
    }

    if (!empty($filterQuery)){
      $this->parameter->setOption('filter', $filterQuery->__toString());
    }

    $azureResult = new AzureResult($this, $this->parameter);
    return $azureResult;
  }

  public function fetch(ParameterAble $optionsParam)
  {
//    var_dump($optionsParam->construct());exit();
    return $this->request->request($this->indexerUri, 'POST', $optionsParam->construct(), $status, false);
  }
}
