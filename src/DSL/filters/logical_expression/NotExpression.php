<?php

namespace Drupal\azure_searchx\DSL\filters\logical_expression;

use Drupal\azure_searchx\DSL\filters\comparison_expression\AbstractComparisonExpression;
use Drupal\azure_searchx\DSL\filters\Expression;

class NotExpression extends AbstractLogicExpression implements Expression
{

  public function keyword()
  {
    return 'not';
  }

  public function type()
  {
    return self::TYPE_SINGLE;
  }

  /**
   * not expression only one value
   * @param Expression $expression
   * @return NotExpression
   */
  public function not(Expression $expression)
  {
    if (count($this->expressions) == 0) {
      $this->expressions[] = $expression;
    }

    return $this;
  }
}
