<?php

namespace Drupal\azure_searchx\DSL\filters\logical_expression;

use Countable;
use Drupal\azure_searchx\DSL\filters\comparison_expression\AbstractComparisonExpression;
use Drupal\azure_searchx\DSL\filters\Expression;
use Drupal\Core\Executable\ExecutableException;
use PhpParser\Node\Expr\Array_;

abstract class AbstractLogicExpression implements Expression, Countable
{
  /**
   * @var Array_<AbstractComparisonExpression>
   */
  protected $expressions;

  public function __construct()
  {
    $this->expressions = [];
  }

  public function count()
  {
    return count($this->expressions);
  }

  public function __toString()
  {
    $type = $this->type();
    if ($type == self::TYPE_SINGLE && count($this->expressions) >= 1) {
      return $this->keyword() . ' (' . $this->expressions[0]->__toString() . ')';//fetch one if expr more than one
    } elseif (($type == self::TYPE_DOUBLE || $type == self::TYPE_MULTI)) {
      if ($this->count() >= 2) {
        $expressions = [];
        foreach ($this->expressions as $expression) {
          $expressions[] = ' (' . $expression->__toString() . ') ';
        }

        return trim(implode($this->keyword(), $expressions));
      } else if ($this->count() >= 1) {
        return $this->expressions[0]->__toString();//fetch one if expr more than one
      }
    }

    return '';
  }
}
