<?php

namespace Drupal\azure_searchx\DSL\filters\logical_expression;

use Drupal\azure_searchx\DSL\filters\comparison_expression\AbstractComparisonExpression;
use Drupal\azure_searchx\DSL\filters\Expression;

class OrExpression extends AbstractLogicExpression implements Expression
{

  public function keyword()
  {
    return 'or';
  }

  public function type()
  {
    return self::TYPE_MULTI;
  }

  /**
   * @param Expression $expression
   * @return OrExpression
   */
  public function or(Expression $expression)
  {
    $this->expressions[] = $expression;
    return $this;
  }
}
