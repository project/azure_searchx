<?php

namespace Drupal\azure_searchx\DSL\filters;

interface KeyValue
{
  function getKey();

  function getValue();
}
