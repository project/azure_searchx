<?php
namespace Drupal\azure_searchx\DSL\filters\comparison_expression;

use Drupal\azure_searchx\DSL\filters\Expression;
use Drupal\azure_searchx\DSL\filters\KeyValue;

abstract class AbstractComparisonExpression implements Expression, KeyValue {
  protected $key;
  protected $value;

  public function __construct($key, $value)
  {
    $this->key = $key;
    $this->value = $value;
  }

  public function getKey()
  {
    return $this->key;
  }

  public function getValue()
  {
    if(is_string($this->value)){
      return "'{$this->value}'";
    }

    return $this->value;
  }

  public function type()
  {
    return self::TYPE_DOUBLE;
  }

  public function __toString()
  {
    return $this->getKey() . ' ' . $this->keyword() . ' ' . $this->getValue();
  }
}
