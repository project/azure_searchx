<?php
namespace Drupal\azure_searchx\DSL\filters\comparison_expression;

use Drupal\azure_searchx\DSL\filters\Expression;

class EqExpression extends AbstractComparisonExpression implements Expression{
  public function __construct($key, $value)
  {
    parent::__construct($key, $value);
  }

  public function keyword()
  {
    return 'eq';
  }
}
