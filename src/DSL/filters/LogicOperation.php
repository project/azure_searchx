<?php

namespace Drupal\azure_searchx\DSL\filters;

interface LogicOperation
{
  //logical expression

  /**
   * @param $expression
   * @return self
   */
  function And($expression);

  /**
   * @param $expression
   * @return self
   */
  function Or($expression);

  /**
   * @param $expression
   * @return self
   */
  function Not($expression);
}
