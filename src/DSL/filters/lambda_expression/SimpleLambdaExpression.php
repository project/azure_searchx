<?php

namespace Drupal\azure_searchx\DSL\filters\lambda_expression;

use Drupal\azure_searchx\DSL\filters\LambdaExpressionInterface;
use Drupal\azure_searchx\DSL\filters\logical_expression\AndExpression;
use Drupal\azure_searchx\DSL\filters\logical_expression\NotExpression;
use Drupal\azure_searchx\DSL\filters\logical_expression\OrExpression;

class SimpleLambdaExpression implements LambdaExpressionInterface
{
  const LogicConnect = 'and';

  protected $andPredicate;

  protected $orPredicate;

  protected $notPredicate;

  protected $alias;

  public function __construct($alias)
  {
    $this->alias = $alias;
    $this->andPredicate = new AndExpression();
    $this->orPredicate = new OrExpression();
    $this->notPredicate = new NotExpression();
  }

  public function keyword()
  {
    return 'lambda';
  }

  public function type()
  {
    return self::TYPE_UNKOWN;
  }

  private function wrapp($value)
  {
    return ' (' . $value . ') ';
  }

  public function __toString()
  {
    $predicates = [];
    if ($this->andPredicate->count() > 0) {
      $predicates[] = $this->wrapp($this->andPredicate->__toString());
    }

    if ($this->orPredicate->count() > 0) {
      $predicates[] = $this->wrapp($this->orPredicate->__toString());
    }

    if ($this->notPredicate->count() > 0) {
      $predicates[] = $this->wrapp($this->notPredicate->__toString());
    }

    return $this->alias . ': ' . trim(implode(self::LogicConnect, $predicates));
  }

  public function And($expression)
  {
    $this->andPredicate->and($expression);
    return $this;
  }

  public function Or($expression)
  {
    $this->orPredicate->or($expression);
    return $this;
  }

  public function Not($expression)
  {
    $this->notPredicate->not($expression);
    return $this;
  }
}

