<?php

namespace Drupal\azure_searchx\DSL\filters;

interface LambdaPredicate
{
  function lambda(LambdaExpressionInterface $query);
}
