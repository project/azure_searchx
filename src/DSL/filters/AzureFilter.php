<?php

namespace Drupal\azure_searchx\DSL\filters;

use Drupal\azure_searchx\DSL\filters\comparison_expression\EqExpression;
use Drupal\azure_searchx\DSL\filters\comparison_expression\GeExpression;
use Drupal\azure_searchx\DSL\filters\comparison_expression\GtExpression;
use Drupal\azure_searchx\DSL\filters\comparison_expression\LeExpression;
use Drupal\azure_searchx\DSL\filters\comparison_expression\LtExpression;
use Drupal\azure_searchx\DSL\filters\comparison_expression\NeExpression;
use Drupal\azure_searchx\DSL\filters\filter_expression\AllExpression;
use Drupal\azure_searchx\DSL\filters\filter_expression\AnyExpression;
use Drupal\azure_searchx\DSL\filters\function_expression\MatchExpression;
use Drupal\azure_searchx\DSL\filters\function_expression\RangeExpression;
use Drupal\azure_searchx\DSL\filters\lambda_expression\SimpleLambdaExpression;
use Drupal\azure_searchx\DSL\filters\logical_expression\AndExpression;
use Drupal\azure_searchx\DSL\filters\logical_expression\NotExpression;
use Drupal\azure_searchx\DSL\filters\logical_expression\OrExpression;

class AzureFilter
{
  protected $andExprs;

  public function __construct()
  {
    $this->andExprs = new AndExpression();
  }

  public function condition($expression)
  {
    $this->andExprs->and($expression);
    return $this;
  }

  public function allGroup($field, $alias, $lambda)
  {
    $expr = new AllExpression($field, $alias);
    $lambdaExpr = new SimpleLambdaExpression($alias);
    $lambdaExpr = $lambda($lambdaExpr);
    $expr->lambda($lambdaExpr);
    return $expr;
  }

  public function anyGroup($field, $alias, $lambda)
  {
    $expr = new AnyExpression($field, $alias);
    $lambdaExpr = new SimpleLambdaExpression($alias);
    $lambda($lambdaExpr);
    $expr->lambda($lambdaExpr);
    return $expr;
  }

  public function orGroupWith($expression)
  {
    $expr = new OrExpression();
    return $expr->or($expression);
  }

  public function orGroup(){
    $expr = new OrExpression();
    return $expr;
  }

  public function notGroup($expression)
  {
    $expr = new NotExpression();
    return $expr->not($expression);
  }

  public static function Eq($field, $value)
  {
    return new EqExpression($field, $value);
  }

  public static function Ge($field, $value)
  {
    return new GeExpression($field, $value);
  }

  public static function Gt($field, $value)
  {
    return new GtExpression($field, $value);
  }

  public static function Le($field, $value)
  {
    return new LeExpression($field, $value);
  }

  public static function Lt($field, $value)
  {
    return new LtExpression($field, $value);
  }

  public static function Ne($field, $value)
  {
    return new NeExpression($field, $value);
  }

  public static function In($field, $value, $delimiter=','){
    return new RangeExpression($field, $value, $delimiter);
  }

  public static function Match($field, $value){
    return new MatchExpression($value, $field);
  }

  public function __toString()
  {
    return $this->andExprs->__toString();
  }
}
