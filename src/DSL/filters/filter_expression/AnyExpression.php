<?php
namespace Drupal\azure_searchx\DSL\filters\filter_expression;

use Drupal\azure_searchx\DSL\filters\Expression;

class AnyExpression extends AbstractFilterExpressionInterface implements Expression{
  public function __construct($field, $alias)
  {
    parent::__construct($field, $alias);
  }

  public function keyword()
  {
    return 'any';
  }
}
