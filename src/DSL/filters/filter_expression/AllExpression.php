<?php

namespace Drupal\azure_searchx\DSL\filters\filter_expression;

use Drupal\azure_searchx\DSL\filters\Expression;

class AllExpression extends AbstractFilterExpressionInterface implements Expression
{
  public function __construct($field, $alias)
  {
    parent::__construct($field, $alias);
  }

  public function keyword()
  {
    return 'all';
  }
}
