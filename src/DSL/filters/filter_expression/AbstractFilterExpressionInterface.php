<?php

namespace Drupal\azure_searchx\DSL\filters\filter_expression;

use Drupal\azure_searchx\DSL\filters\Expression;
use Drupal\azure_searchx\DSL\filters\LambdaPredicate;
use Drupal\azure_searchx\DSL\filters\LambdaExpressionInterface;

abstract class AbstractFilterExpressionInterface implements Expression, LambdaPredicate
{
  protected $field;

  protected $lambdaResult;

  public function __construct($field, $alias)
  {
    $this->field = $field;
  }

  public function type()
  {
    return self::TYPE_SINGLE;
  }

  public function __toString()
  {
    return $this->field . '/' . $this->keyword() . '(' . $this->lambdaResult . ')';
  }

  public function lambda(LambdaExpressionInterface $query)
  {
    $this->lambdaResult = $query->__toString();
  }
}
