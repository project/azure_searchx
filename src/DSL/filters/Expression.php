<?php

namespace Drupal\azure_searchx\DSL\filters;

interface Expression
{
  const TYPE_SINGLE = 1;
  const TYPE_MULTI = 2;
  const TYPE_DOUBLE = 3;
  const TYPE_UNKOWN = 4;

  /**
   * any expression is unique
   * @return mixed
   */
  function keyword();

  /**
   *  return TYPE_SINGLE or TYPE_DOUBLE
   * @return mixed
   */
  function type();

  function __toString();
}
