<?php

namespace Drupal\azure_searchx\DSL\filters\function_expression;

use Drupal\azure_searchx\DSL\filters\Expression;

class MatchExpression implements Expression
{
  protected $value;

  protected $fields;

  protected $queryType;

  protected $searchMode;

  public function __construct($value, $fields)
  {
    $this->value = $value;
    $this->fields = $fields;
    $this->queryType = 'simple';
    $this->searchMode = 'any';
  }


  public function keyword()
  {
    return 'search.ismatch';
  }

  function type()
  {
    return self::TYPE_UNKOWN;
  }

  function __toString()
  {
    return $this->keyword() . '(' . "'$this->value', '" . $this->fields . "', '" . $this->queryType . "', '" . $this->searchMode . "')";
  }
}
