<?php

namespace Drupal\azure_searchx\DSL\filters\function_expression;

use Drupal\azure_searchx\DSL\filters\Expression;

class RangeExpression implements Expression
{
  protected $delimiter;

  protected $field;

  protected $values;

  public function __construct($field, $values, $delimiter=',')
  {
    $this->field = $field;
    $this->values = $values;
    $this->delimiter = $delimiter;
  }

  public function keyword()
  {
    return 'search.in';
  }

  public function type()
  {
    return self::TYPE_UNKOWN;
  }

  public function __toString()
  {
    return $this->keyword() . '(' . $this->field . ', ' . "'$this->values', '" . $this->delimiter . "')";
  }
}
