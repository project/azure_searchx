<?php
namespace Drupal\azure_searchx\DSL;

interface IPage{
  /**
   * @param $page
   * @param $orderBy
   * @return PageResultSet
   */
  function pagination($page, $orderBy='');
}
