<?php
namespace Drupal\azure_searchx\DSL;

interface SearchQueryAble{
  function fetch(ParameterAble $optionsParam);
}
