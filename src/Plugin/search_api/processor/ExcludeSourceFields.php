<?php

namespace Drupal\azure_searchx\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\QueryInterface;

/**
 * Search api processor.
 *
 * @SearchApiProcessor(
 *   id = "exclude_source_fields",
 *   label = @Translation("Exclude source fields"),
 *   description = @Translation("Exclude certain source fields from search results"),
 *   stages = {
 *     "preprocess_query" = -20
 *   }
 * )
 */
class ExcludeSourceFields extends ProcessorPluginBase implements PluginFormInterface {
  use PluginFormTrait;

  /**
   * BuildConfigurationForm function.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state array.
   *
   * @return mixed
   *   Form array
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    return $form;
  }

  /**
   * Process search query.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   Query.
   */
  public function preprocessSearchQuery(QueryInterface $query) {
  }

}
