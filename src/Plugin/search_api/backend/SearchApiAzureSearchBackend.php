<?php

namespace Drupal\azure_searchx\Plugin\search_api\backend;

use Drupal\azure_searchx\AzureSearch\Objects\AzureDocument;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\azure_searchx\AzureSearch\Request\AzureRequest;
use Drupal\azure_searchx\AzureSearch\Objects\AzureIndex;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValue;
use Drupal\search_api\DataType\DataTypePluginManager;

/**
 * Azure Search API Backend definition.
 *
 * @SearchApiBackend(
 *   id = "azure_searchx",
 *   label = @Translation("Azure search"),
 *   description = @Translation("Index items using an azure search server.")
 * )
 */
class SearchApiAzureSearchBackend extends BackendPluginBase implements
    PluginFormInterface {

  use PluginFormTrait;

  /**
   * Set a large integer to be the size for a "Hard limit" value of "No limit".
   */
  const FACET_NO_LIMIT_SIZE = 10000;

  /**
   * Azure search settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $azureSearchSettings;

  /**
   * Form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The azure request class.
   *
   * @var \Drupal\azure_searchx\AzureSearch\Request\AzureRequest
   */
  protected $request;

  /**
   * The type manager service.
   *
   * @var \Drupal\search_api\DataType\DataTypePluginManager
   */
  protected $pluginManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  public function config() {

  }

  /**
   * SearchApiAzureSearchBackend constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    FormBuilderInterface $formBuilder,
    ModuleHandlerInterface $moduleHandler,
    DataTypePluginManager $pluginManager,
    MessengerInterface $messenger,
    LoggerChannel $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (empty($this->configuration['azure']['server'])) {
      $this->configuration['azure']['server'] = $this->defaultConfiguration();
    }

    $this->formBuilder = $formBuilder;
    $this->moduleHandler = $moduleHandler;
    $this->pluginManager = $pluginManager;
    $this->request = new AzureRequest(
      $this->configuration['azure']['server']['azure_searchx_host'],
      $this->configuration['azure']['server']['subscription_key']
    );

    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('module_handler'),
      $container->get('plugin.manager.search_api.data_type'),
      $container->get('messenger'),
      $container->get('logger.factory')->get('azure_searchx')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $params['azure']['server'] = [
      'azure_searchx_host' => '',
      'subscription_key'  => '',
      'query_logging'  => 0,
    ];

    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
  FormStateInterface $form_state
  ) {
    $form['azure']['server']['subscription_key'] = [
      '#title'         => $this->t('Api Key'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#default_value' => $this->configuration['azure']['server']['subscription_key'],
    ];

    $form['azure']['server']['azure_searchx_host'] = [
      '#title'         => $this->t('Azure search host'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#default_value' => $this->configuration['azure']['server']['azure_searchx_host'],
    ];

    $form['query_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Add a raw query to the log'),
      '#default_value' => array_key_exists('query_logging', $this->configuration)? $this->configuration['query_logging']: $this->configuration['azure']['server']['query_logging'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedFeatures() {
    // First, check the features we always support.
    return [
      // 'search_api_autocomplete',.
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info = [];

    $info[] = [
      'label' => $this->t('Azure host URI'),
      // @codingStandardsIgnoreLine
      'info'  => Link::fromTextAndUrl(
        $this->configuration['azure']['server']['azure_searchx_host'],
        Url::fromUri($this->configuration['azure']['server']['azure_searchx_host'])
      ),
    ];

    $con_state = ($this->request->requestCode("/") == 200) ? 'ok' : 'error';

    $info[] = [
      'label' => $this->t('Connection'),
      // @codingStandardsIgnoreLine
      'info'  => $this->t($con_state),
    ];

    $httpcode = -1;
    $resp = $this->request->request('indexes', 'GET', [], $httpcode);
    $indexes = "";
    if (!empty($resp)){
      foreach ($resp->value as $item) {
        // @codingStandardsIgnoreLine
        $indexes .= $this->t($item->name) . ", ";
      }
    }
    $info[] = [
      'label' => $this->t('Indexes on server'),
      // @codingStandardsIgnoreLine
      'info'  => $this->t($indexes),
    ];
    $info[] = [
      'label' => $this->t('Query debug'),
      // @codingStandardsIgnoreLine
      'info'  => $this->t( $this->configuration['query_logging'] == 0 ? 'disable':'enable' ),
    ];

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    $this->updateIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    $index_obj = new AzureIndex($this->pluginManager, $index->get('name'));
    $index_obj->addField('id', 'string', TRUE);
    $index_obj->addField('itemId', 'string');
    $index_obj->addField('language', 'string');
    $scoring = new \stdClass();
    foreach ($index->get('field_settings') as $key => $field) {
      $index_obj->addField($key, $field['type']);
      if (!empty($field['boost'])) {
        $scoring->{$key} = $field['boost'];
      }
    }
    $index_obj->addScoringProfile('apiBoost', $scoring);

    $httpcode = -1;
    $resp = $this->request->request('indexes/' . $index->get('name'), 'PUT',
      $index_obj->getDefinition(), $httpcode);

    if (!($httpcode >= 200 && $httpcode < 205)) {
      $this->messenger->addError($this->t("something goes wrong, data definition is NOT changed. @msg",
        ["@msg" => $resp->error->message]));
    }

    return $resp;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $this->removeIndex($index);
    $this->addIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    $httpcode = -1;
    $this->request->request('indexes/' . $index->get('name'), 'DELETE', [], $httpcode);
  }

  /**
   * {@inheritdoc}
   */
  protected function getValuebyType($field_type, $value) {
    switch ($field_type) {
      case 'string':
        $result = (string) $value;
        break;

      case 'text':
        if ($value instanceof TextValue) {
          $result = $value->toText();
        }
        else {
          $result = $value;
        }
        break;

      case 'boolean':
        $result = ($value == "1") ? "true" : "false";
        break;

      default:
        $result = $value;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $httpcode = -1;
    $documents = new AzureDocument("mergeOrUpload");

    $indexed = [];
    foreach ($items as $id => $item) {
      $str_id = str_replace(":", "_", $id);
      $str_id = str_replace("/", "_", $str_id);
      $values = ['id' => $str_id];
      $values['itemId'] = $id;
      $values['language'] = $item->getLanguage();
      $indexed[$str_id] = $id;
      foreach ($item as $name => $field) {
        if (!isset($field->getValues()[0])) {
          $values[$name] = $this->isFieldCollection($field->getType()) ? [] : NULL;
          continue;
        }

        if (!$this->isFieldCollection($field->getType())) {
          $values[$name] = $this->getValuebyType($field->getType(), $field->getValues()[0]);
          continue;
        }

        foreach ($field->getValues() as $value) {
          $values[$name][] = $this->getValuebyType($field->getType(), $value);
        }
      }

      $documents->addDocument($values);
    }

    $successed = [];
    $resp = $this->request->request(
      'indexes/' . $index->get('name') . "/docs/index", 'POST',
      $documents->getDocuments(),
      $httpcode
    );

    if (!empty($resp)){
      foreach ($resp->value as $status) {
        if ($status->statusCode == 200 | $status->statusCode == 201) {
          $successed[] = $indexed[$status->key];
        }
      }
    }

    return $successed;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index = NULL, array $ids = []) {
    if (!count($ids)) {
      return;
    }

    $documents = new AzureDocument("delete");

    foreach ($ids as $id) {
      $str_id = str_replace(":", "_", $id);
      $str_id = str_replace("/", "_", $str_id);
      $documents->addDocument(['id' => $str_id]);
    }

    $httpcode = -1;
    $path = 'indexes/' . $index->get('name') . "/docs/index";
    $this->request->request($path, 'POST', $documents->getDocuments(), $httpcode);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSearchString(&$search, QueryInterface $query) {
    $search_string = "";
    $act = "";

    $keys = $query->getKeys();
    if (is_string($keys)) {
      $search_string = $keys;
      $search->searchMode = "any";
    }
    elseif (isset($keys['#negation']) && $keys['#negation'] == TRUE) {
      $search->searchMode = "any";
      $act = "-";
    }
    else {
      $search->searchMode = (isset($keys['#conjunction']) && $keys['#conjunction'] == 'and') ? "all" : "any";
    }

    if (!empty($keys) && count($keys)) {
      foreach ($keys as $key => $val) {
        if (is_numeric($key)) {
          $search_string .= ($search_string == "" ? '' : ' ') . $act . trim($val) . "*";
        }
      }
    }

    $search->search = $search_string;
  }

  /**
   * {@inheritdoc}
   */
  protected function isFieldCollection($type) {
    $plugin = $this->pluginManager->getDefinition($type);
    if (method_exists($plugin['class'], 'isCollection')) {
      return $plugin['class']::isCollection();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldFunction($field_name, $type, $value, $operator) {
    $plugin = $this->pluginManager->getDefinition($type);
    if (method_exists($plugin['class'], 'fieldFilterFunction')) {
      return $plugin['class']::fieldFilterFunction($field_name, $value, $operator);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function inString($array, $field, $operator, $index) {
    $result = '';
    $add = '';

    $op = 'eq';

    switch (strtolower($operator)) {
      case "not in":
        $op = 'ne';
        break;
    }

    $cnt = 0;

    $fields = $index->getFields();
    foreach ($array as $item) {
      $type = $fields[$field]->getType();
      if ($this->isFieldCollection($type)) {
        $result .= $add . '(' . $field . "/any(v: v " . $op . " '" . $item . "'))";
      }
      else {
        $result .= $add . $field . " " . $op . " '" . $item . "'";
      }
      if ($add == '') {
        $add = ' or ';
      }

      $cnt++;
    }
    return ($cnt > 1) ? '(' . $result . ')' : $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function inInt($array, $field, $operator, $index) {
    $result = '';
    $add = '';

    $op = 'eq';

    $cnt = 0;

    switch (strtolower($operator)) {
      case "not in":
        $op = 'ne';
        break;
    }

    $fields = $index->getFields();
    foreach ($array as $item) {
      $type = $fields[$field]->getType();
      if ($this->isFieldCollection($type)) {
        $result .= $add . '(' . $field . "/any(v: v " . $op . " '" . $item . "'))";
      }
      else {
        $result .= $add . $field . " " . $op . " '" . $item . "'";
      }
      if ($add == '') {
        $add = ' or ';
      }

      $cnt++;
    }
    return ($cnt > 1) ? '(' . $result . ')' : $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSubConditionString($condition_group, $index) {
    $conditions = $condition_group->getConditions();
    $add = '';
    $result = '';
    $cnt = 0;
    foreach ($conditions as $condition) {
      $cnt++;
      $value = $condition->getValue();
      $field = $condition->getField();
      $operator = $condition->getOperator();
      $type = $index->getField($field)->getType();
      $this->moduleHandler->alter('azure_searchx_filter_' . $type, $value);
      if ($field_filter = $this->getFieldFunction($field, $type, $value, $operator)) {
        $result .= $add . $field_filter;
      }
      else {
        switch ($type) {
          case 'string':
            $result .= $add . $this->inString($value, $field, $operator, $index);
            break;

          case 'integer':
            $result .= $add . $this->inInt($value, $field, $operator, $index);
            break;

          default:
            break;
        }
      }
      if ($add == '') {
        $add = ' ' . strtolower($condition_group->getConjunction()) . ' ';
      }
    }

    if ($result == '') {
      return '';
    }

    return ($cnt > 1) ? '(' . $result . ')' : $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConditionString($condition_group, $index) {
    $conditions = $condition_group->getConditions();
    $add = '';
    $result = '';
    $cnt = 0;
    foreach ($conditions as $condition) {
      $cnt++;
      $result .= $add . $this->getSubConditionString($condition, $index);

      if ($add == '') {
        $add = ' ' . strtolower($condition_group->getConjunction()) . ' ';
      }
    }

    if ($result == '') {
      return '';
    }
    return ($cnt > 1) ? '(' . $result . ')' : $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function isScoringProfile($index) {
    foreach ($index->get('field_settings') as $field) {
      if (!empty($field['boost'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    $search = new \stdClass();
    $search->count = TRUE;
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $search_result = $query->getResults();
    $index = $query->getIndex();
    $sorts = $query->getSorts();

    $this->buildSearchString($search, $query);
    $fields = $query->getFulltextFields();
    $search->searchFields = "";
    if (!empty($fields) && count($fields)) {
      foreach ($fields as $field) {
        ($search->searchFields != "") && ($search->searchFields .= ", ");
        $search->searchFields .= $field;
      }
    }

    if ($this->isScoringProfile($index)) {
      $search->scoringProfile = 'apiBoost';
    }

    if ($filter = $this->getConditionString($query->getConditionGroup(), $index)) {
      $filter = "(language eq '" . $language . "'" . ') and ' . $filter;
    }
    else {
      $filter = "language eq '" . $language . "'";
    }
    $search->filter = $filter;

    $separator = "";
    $sort_string = "";
    foreach ($sorts as $creteria => $sort) {
      switch ($creteria) {
        case "search_api_relevance":
          $sort_string = $sort_string . $separator . "search.score() " . strtolower($sort);
          break;

        default:
          $sort_string = $sort_string . $separator . $creteria . " " . strtolower($sort);
          break;
      }

      if ($sort_string != "" && $separator == "") {
        $separator = ", ";
      }
    }
    if ($sort_string != "") {
      $search->orderby = $sort_string;
    }

    $options = $query->getOptions();
    if (isset($options["limit"])) {
      $search->top = $options["limit"];
      $search->skip = $options["offset"];
    }

    $httpcode = -1;
    $path = 'indexes/' . $index->get('name') . "/docs/search";

    if (!empty($this->configuration['query_logging'])) {
      $this->logger->debug(json_encode($search));
    }

    $resp = $this->request->request($path, 'POST', $search, $httpcode);
    if (!empty($resp) && count($resp->value)) {
      $search_result->setResultCount(count($resp->value));
      foreach ($resp->value as $res) {
        $item = $this->getFieldsHelper()->createItem($index, $res->itemId);
        $item->setExtraData('extra', $res);
        $search_result->addResultItem($item);
      }
      $search_result->setResultCount($resp->{'@odata.count'});
    }

    $this->moduleHandler->alter('azure_searchx_results', $query);
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable() {
    return $this->request->requestCode("/") == 200;
  }

  /**
   * This allows subclasses to apply custom changes before the query is sent.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The \Drupal\search_api\Query\Query object representing the executed
   *   search query.
   */
  protected function preQuery(QueryInterface $query) {
  }

  /**
   * This allows subclasses to apply custom changes after the query is sent.
   *
   * @param \Drupal\search_api\Query\ResultSetInterface $results
   *   The results array that will be returned for the search.
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The \Drupal\search_api\Query\Query object representing the executed
   *   search query.
   * @param object $response
   *   The response object returned by Elastic Search.
   */
  protected function postQuery(
    ResultSetInterface $results,
    QueryInterface $query,
    $response
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDataType($type) {
    return in_array($type, ['object']);
  }

  /**
   * Implements __sleep()
   *
   * Prevents closure serialization error on search_api server add form.
   */
  public function __sleep() {
    return [];
  }

}
