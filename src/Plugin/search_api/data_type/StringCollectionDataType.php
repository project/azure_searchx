<?php

namespace Drupal\azure_searchx\Plugin\search_api\data_type;

use Drupal\azure_searchx\AzureDataTypeInterface;
use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a string collection data type.
 *
 * @SearchApiDataType(
 *   id = "azure_string_collection",
 *   label = @Translation("String collection"),
 *   fallback_type = "string",
 *   default = "true"
 * )
 */
class StringCollectionDataType extends DataTypePluginBase implements AzureDataTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function getAzureType() {
    return "Collection(Edm.String)";
  }

  /**
   * {@inheritdoc}
   */
  public static function isCollection() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldFilterFunction($field_name, $value, $operator) {
    return "((${field_name}/any(v: v ${operator} '${value}')) or (not ${field_name}/any()))";
  }

}
