<?php

/**
 * @file
 * Hooks related to azure_searchx module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided in \Drupal\azure_searchx\Plugin\search_api\backend.
 *
 * @param $value
 *   Alter the value of a specific search filter.
 */
function hook_azure_searchx_filter_FILTER_TYPE_alter(&$value) {
  $value = "Some altered value";
}

/**
 * Alter the information provided in \Drupal\azure_searchx\Plugin\search_api\backend.
 *
 * @param \Drupal\search_api\Query $query
 *   Search api query with result items.
 */
function hook_azure_searchx_results_alter(Query $query) {
  $results = $query->getResults();
  foreach ($results as &$item) {
    $type = $item->getExtraData('extra')->type;
    //Some actions on results
  }
  $results->setResultItems($results);
}

/**
 * @} End of "addtogroup hooks".
 */
